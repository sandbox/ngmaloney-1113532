<?php

/**
 * Block Anonymous users from accessing a particular context
 */
class context_reaction_restrict_roles extends context_reaction {
  function options_form($context) {
    $roles = user_roles();
    $values = $this->fetch_from_context($context);
    $default_roles = (isset($values['roles'])) ? $this->parse_role_values($values['roles']) : array(); 
    $default_path = (isset($values['path'])) ? $values['path'] : '';
    $form = array();
    $form = array(
      '#tree' => TRUE,
      '#title' => t('Role based access'),
      'roles' => array(
        '#type' => 'checkboxes',
        '#title' => 'Roles',
        '#default_value' => $default_roles,
        '#options' => $roles,
        '#description' => t('Select the roles you wish to block from viewing this context'),
      ),
      //TODO: ADD VALIDATION!!!
      'path' => array(
        '#type' => 'textfield',
        '#title' => 'Path',
        '#default_value' => $default_path,
        '#description' => t('Users will either see an access denied message or redirect to this path'),
      ),
    );
    return $form;
  }
  function execute(&$vars) {
    $contexts = context_active_contexts();
    global $user;
    foreach ($contexts as $k => $v) {
      if (!empty($v->reactions[$this->plugin])) {
        $values = $this->fetch_from_context($v);
        $roles = $this->parse_role_values($values['roles']);
        $path = (!empty($values['path'])) ? $values['path'] : false;
        foreach($roles as $idx => $rid) {
          if(in_array($rid, array_keys($user->roles))) {
            if($path) {
              drupal_goto($path);
            }
            else {
              drupal_access_denied();
              drupal_exit();
            }
          }
        } 
      }
    }
  }

  /**
   * Extracts role ID  from from context settings form
   *
   * TODO: Eliminate this function.
   */
  protected function parse_role_values($vals) {
    if(!is_array($vals)) {
      return array();
    }
    $rvals = array();
    foreach($vals as $k => $v) {
      if($v != 0) {
        $rvals[] = $v;
      }
    }
    return $rvals;
  }
}

